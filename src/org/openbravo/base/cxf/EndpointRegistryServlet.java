/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf;

import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openbravo.base.cxf.security.HttpBasicAuthenticator;
import org.openbravo.base.exception.OBSecurityException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.service.web.UserContextCache;

/**
 * This class allows to publish the SOAP web services with the CXF servlet transport, without
 * touching any Spring stuff.
 */
public class EndpointRegistryServlet extends CXFNonSpringServlet {

  private static final Logger log = LogManager.getLogger();
  private static final long serialVersionUID = 1L;

  @Inject
  private HttpBasicAuthenticator authenticator;

  @Inject
  private EndpointPublisher publisher;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException {

    Optional<String> userId = authenticator.authenticate(request);

    if (userId.isPresent()) {
      checkWebServicesEnabled(userId.get());
      // user authenticated, proceed with the GET request
      super.doGet(request, response);
    } else {
      // not logged in
      setResponseAsUnauthorized(response, request);
    }
  }

  private void checkWebServicesEnabled(String userId) {
    OBContext.setOBContext(UserContextCache.getInstance().getCreateOBContext(userId));
    if (OBContext.getOBContext().isWebServiceEnabled()) {
      return;
    }
    log.error("User {} with role {} is trying to access to non granted SOAP web service",
        OBContext.getOBContext().getUser(), OBContext.getOBContext().getRole());
    throw new OBSecurityException(
        "Web Services are not granted to " + OBContext.getOBContext().getRole() + " role");
  }

  private void setResponseAsUnauthorized(HttpServletResponse response, HttpServletRequest request) {
    if (!"false".equals(request.getParameter("auth"))) {
      response.setHeader("WWW-Authenticate", "Basic realm=\"Openbravo\"");
    }
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
  }

  @Override
  public void loadBus(ServletConfig servletConfig) {
    super.loadBus(servletConfig);

    // The Bus is the backbone of the CXF architecture: it manages extensions and acts as an
    // interceptor provider
    Bus defaultBus = getBus();
    BusFactory.setDefaultBus(defaultBus);

    publisher.publishSoapWebServices();
  }
}
