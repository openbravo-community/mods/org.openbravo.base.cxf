/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf;

import java.util.Arrays;
import java.util.List;

import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.message.Message;
import org.openbravo.base.cxf.security.HttpBasicAuthenticationInterceptor;
import org.openbravo.base.provider.OBSingleton;

/**
 * Classes implementing this interface will be used to register automatically SOAP web services on
 * server startup. By default those web services will be secured using basic HTTP Authentication.
 */
public interface SOAPWebServiceRegister {

  /**
   * @return a List of {@code SOAPWebService} with the information of the SOAP web services to be
   *         registered on startup.
   */
  List<SOAPWebService<? extends OBSingleton>> getSOAPWebServices();

  /**
   * @return the list of incoming interceptors that will be associated to each web service
   *         registered with this class. By default it returns a list containing the
   *         {@link HttpBasicAuthenticationInterceptor} interceptor which provides basic HTTP
   *         authentication.
   */
  default List<Interceptor<? extends Message>> getInterceptors() {
    return Arrays.asList(new HttpBasicAuthenticationInterceptor());
  }
}
