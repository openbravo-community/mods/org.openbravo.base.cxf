/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.security;

import static org.apache.cxf.transport.http.AbstractHTTPDestination.HTTP_REQUEST;
import static org.openbravo.authentication.AuthenticationManager.STATELESS_REQUEST_PARAMETER;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.interceptor.AbstractInDatabindingInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openbravo.authentication.AuthenticationManager;
import org.openbravo.authentication.basic.DefaultAuthenticationManager;
import org.openbravo.base.session.OBPropertiesProvider;
import org.openbravo.base.util.OBClassLoader;
import org.openbravo.dal.core.OBContext;
import org.openbravo.service.web.UserContextCache;

/**
 * An incoming interceptor that provides HTTP Basic Access Authentication.
 */
public class HttpBasicAuthenticationInterceptor extends AbstractInDatabindingInterceptor {

  private static final Logger log = LogManager.getLogger();

  private AuthenticationManager authenticationManager;
  private boolean isStatelessRequest;

  /**
   * Creates a new HttpBasicAuthenticationInterceptor object. By default it configures the web
   * service requests as stateless.
   */
  public HttpBasicAuthenticationInterceptor() {
    this(true);
  }

  /**
   * Creates a new HttpBasicAuthenticationInterceptor object.
   * 
   * @param isStatelessRequest
   *          used to define if the web service requests are stateless or not. If this parameter is
   *          {@code false} then each web service call will create and use an HTTP session. If it is
   *          {@code true}, then no HTTP session will be created.
   */
  public HttpBasicAuthenticationInterceptor(boolean isStatelessRequest) {
    super(Phase.RECEIVE);
    this.isStatelessRequest = isStatelessRequest;
    authenticationManager = getAuthenticationManager();
  }

  private AuthenticationManager getAuthenticationManager() {
    String authClass = OBPropertiesProvider.getInstance()
        .getOpenbravoProperties()
        .getProperty("authentication.class",
            "org.openbravo.authentication.basic.DefaultAuthenticationManager");
    if (StringUtils.isBlank(authClass)) {
      return new DefaultAuthenticationManager();
    }
    try {
      return (AuthenticationManager) OBClassLoader.getInstance()
          .loadClass(authClass)
          .getDeclaredConstructor()
          .newInstance();
    } catch (Exception e) {
      log.warn("Could not create instance of authentication manager class {}. Using default one.",
          authClass);
      return new DefaultAuthenticationManager();
    }
  }

  @Override
  public void handleMessage(Message message) throws Fault {
    AuthorizationPolicy policy = message.get(AuthorizationPolicy.class);
    if (policy == null) {
      // Policy is not set: the credentials are not present. Return a 401 (Unauthorized) error
      // to the client to indicate that authentication is required.
      log.debug("SOAP WS accessed by unauthenticated user");
      throw new HttpUnauthorizedException();
    }
    // Verify the credentials
    String userId = authenticate((HttpServletRequest) message.get(HTTP_REQUEST));
    if (userId == null) {
      // Wrong credentials. Return a 403 (Forbidden) error.
      log.debug("SOAP WS accessed with wrong credentials by user with ID {}", userId);
      throw new HttpForbiddenException();
    }

    // Set the OBContext
    OBContext.setOBContext(UserContextCache.getInstance().getCreateOBContext(userId));
  }

  private String authenticate(HttpServletRequest request) {
    if (isStatelessRequest) {
      request.setAttribute(STATELESS_REQUEST_PARAMETER, "true");
    }
    return authenticationManager.webServiceAuthenticate(request);

  }
}
