/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.apache.cxf.Bus;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.message.Message;

/**
 * A class that encapsulates a JAX-WS service in order to configure it and set the data required to
 * consume a SOAP web service.
 */
public class SOAPClient<T> {

  private T service;
  private Class<T> clazz;
  private URL wsdl;
  private QName name;
  private T serviceImpl;
  private Map<String, Object> requestData;
  private List<Interceptor<? extends Message>> interceptors;

  /**
   * Constructs a new SOAPClient with the service information.
   * 
   * @param clazz
   *          Specifies the service endpoint interface (SEI)
   * @param wsdl
   *          URL for the WSDL document location for the service
   * @param targetNamespace
   *          the target namespace of the service
   * @param serviceName
   *          the name of the service
   * @throws InvalidSOAPClientException
   *           if the provided wsdl String can not be used to build a valid URL object
   */
  public SOAPClient(Class<T> clazz, String wsdl, String targetNamespace, String serviceName) {
    this.clazz = clazz;
    try {
      this.wsdl = new URL(wsdl);
    } catch (MalformedURLException ex) {
      throw new InvalidSOAPClientException(ex.getMessage());
    }
    this.name = new QName(targetNamespace, serviceName);
  }

  /**
   * Constructs a new SOAPClient by directly using an already generated client.
   * 
   * @param serviceImpl
   *          An instance of a service client. For example, it can be an instance of an
   *          wsdl2java-generated class.
   */
  public SOAPClient(T serviceImpl) {
    this.serviceImpl = serviceImpl;
  }

  /**
   * Sets the information that will be included into the context that is used to initialize the
   * message context for request messages.
   * 
   * @param data
   *          The data to be added into the request context.
   * @return this OBQuery instance, for method chaining.
   */
  protected SOAPClient<T> setRequestData(Map<String, Object> data) {
    this.requestData = data;
    return this;
  }

  /**
   * Sets the list of interceptors that will be added to the service outgoing interceptor chain.
   * 
   * @param outInterceptors
   *          The list of outgoing interceptors to be associated to the service.
   */
  protected SOAPClient<T> setInterceptors(List<Interceptor<? extends Message>> outInterceptors) {
    this.interceptors = outInterceptors;
    return this;
  }

  /**
   * @return the service instance configured with the provided settings and that can be used to
   *         invoke SOAP services.
   */
  public T getService() {
    try {
      if (service != null) {
        return service;
      }
      if (serviceImpl != null) {
        service = serviceImpl;
      } else {
        // Retrieves the service through a JAX-WS proxy using the SEI. CXF should take care of
        // configuring this proxy accordingly.
        service = Service.create(wsdl, name).getPort(clazz);
      }
      addRequestData();
      addInterceptors();
      return service;
    } catch (Exception ex) {
      throw new InvalidSOAPClientException(ex.getMessage());
    }
  }

  private void addRequestData() {
    if (requestData == null || requestData.isEmpty()) {
      return;
    }
    BindingProvider bindingProvider = (BindingProvider) service;
    bindingProvider.getRequestContext().putAll(requestData);
  }

  private void addInterceptors() {
    if (interceptors == null) {
      return;
    }
    Bus bus = ClientProxy.getClient(service).getBus();
    interceptors.stream().forEach(interceptor -> bus.getOutInterceptors().add(interceptor));
  }
}
