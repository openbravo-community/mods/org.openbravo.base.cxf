/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf.util;

import java.io.OutputStream;
import java.util.Collections;
import java.util.Set;

import javax.xml.stream.XMLStreamWriter;

import org.apache.cxf.interceptor.AttachmentOutInterceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.staxutils.StaxUtils;

/**
 * An abstract outgoing interceptor that can extended to define parts of the SOAP message that
 * should not be parsed. Those parts will be included as CDATA sections.
 */
public abstract class XMLCharacterEscapeInterceptor extends AbstractPhaseInterceptor<Message> {

  public XMLCharacterEscapeInterceptor() {
    super(Phase.PRE_STREAM);
    addAfter(AttachmentOutInterceptor.class.getName());
  }

  @Override
  public void handleMessage(Message message) {
    // Required to make CDATA work
    message.put("disable.outputstream.optimization", Boolean.TRUE);
    OutputStream os = message.getContent(OutputStream.class);
    message.setContent(XMLStreamWriter.class, getCDataXMLStreamWriter(os));
  }

  private CDataXMLStreamWriter getCDataXMLStreamWriter(OutputStream outputStream) {
    XMLStreamWriter writer = StaxUtils.createXMLStreamWriter(outputStream);
    return new CDataXMLStreamWriter(writer, getCDATAElements(), getContentToRemove());
  }

  /**
   * @return a Set of Strings with the elements whose content should be enclosed into a CDATA
   *         section.
   */
  protected abstract Set<String> getCDATAElements();

  /**
   * @return a Set of Strings that will be removed from the XML content of the Message if they are
   *         present. By default it returns an empty set (nothing will be removed).
   */
  protected Set<String> getContentToRemove() {
    return Collections.emptySet();
  }
}
