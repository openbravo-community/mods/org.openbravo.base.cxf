/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2019 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package org.openbravo.base.cxf;

import org.openbravo.base.provider.OBProvider;
import org.openbravo.base.provider.OBSingleton;

/**
 * A class used to define the required information to register a SOAP web service.
 */
public class SOAPWebService<T extends OBSingleton> {

  private String address;
  private Class<T> implementorClass;

  /**
   * Base constructor
   * 
   * @param address
   *          The web service address. It will be published at:
   *          http://context_url/org.openbravo.base.cxf/{address}
   * @param implementorClass
   *          The class of the web service implementor. This class should implement the
   *          {@link OBSingleton} interface.
   */
  private SOAPWebService(String address, Class<T> implementorClass) {
    this.address = address;
    this.implementorClass = implementorClass;
  }

  /**
   * @return The web service address. This is a String that will be appended to the base address to
   *         generate the final web service address.
   */
  String getAddress() {
    return address;
  }

  /**
   * @return The web service implementor.
   */
  T getImplementor() {
    return OBProvider.getInstance().get(implementorClass);
  }

  /**
   * Creates a new SOAPWebService instance.
   * 
   * @param address
   *          The web service address. It will be published at:
   *          http://context_url/org.openbravo.base.cxf/{address}
   * @param clazz
   *          The class of the web service implementor. This class should implement the
   *          {@link OBSingleton} interface.
   * 
   * @return a new SOAPWebService instance.
   */
  public static <E extends OBSingleton> SOAPWebService<E> createSOAPWebService(String address,
      Class<E> clazz) {
    return new SOAPWebService<>(address, clazz);
  }
}
